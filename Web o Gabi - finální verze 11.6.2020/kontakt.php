<!doctype html>
<html>
<head>
	<meta http-equiv="Content Type" content="text/html;charset=UTF-8">
	<title>Kontaktní formulář</title>
</head>
<body>
	<p>Můžeš mě kontaktovat pomocí formuláře níže :</p>
	<?php
	    if ($hlaska)
	        echo("<p>" . $hlaska . "</p>");
	?>
	<form method="POST">
		<table>
			<tr>
				<td style="font-weight: bold; font-size: 1.5em">Vaše jméno:</td>
				<td><input type="text" name="jmeno"></td>
			</tr>
			<tr>
				<td style="font-size: 1.5em; font-weight: bold">Vaše email:</td>
				<td><input type="email" name="email"></td>
			</tr>
			<tr>
				<td style="font-weight: bold; font-size: 1.5em">Aktuální rok:</td>
				<td><input type="number" name="rok"></td>
			</tr>
		</table>
		<textarea name="zprava"></textarea><br />
		<input type="submit" value="Odeslat" style="font-size: 1.5em; font-weight: bold">
	</form>
	<?php
	mb_internal_encoding("UTF-8")

	$hlaska = '';
	if ($_POST)
	{
		if (isset($_POST)['jmeno']] && $_POST['jmeno']] && isset($_POST)['email']] && $_POST['email']] && isset($_POST)['zprava']] && $_POST['zprava']] && isset($_POST)['rok']] && $_post['rok'] == date('Y'))
		{
			$hlavicka = 'From:'. $_POST['email']
			$hlavicka .="\nMIME-Version: 1.0\n"
			$hlavicka .= "Content-Type: text/html; charset=\"utf-8\"\n";
			$adresa = 'gabriela.stiburkova@educanet.cz';
			$predmet = 'Nová zpráva z mailformu';
			$uspech = mb_send_mail($adresa, $predmet, $_POST['zprava'], $hlavicka);
if ($uspech)
{
    $hlaska = 'Email byl úspěšně odeslán, budu se snažit co nejdříve odvědět.';
}
else
    $hlaska = 'Email se bohužel nepodařilo odeslat. Zkus to prosím znovu adresu.';
		}
	}
	?>
</body>
</html>