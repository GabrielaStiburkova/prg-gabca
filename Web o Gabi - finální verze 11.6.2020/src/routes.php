<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'index.latte');
})->setName('index');

$app->get('/letadla', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'letadla.latte');
})->setName('letadla');

$app->get('/cestovani', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'cestovani.latte');
})->setName('cestovani');

$app->get('/anime', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'anime.latte');
})->setName('anime');

$app->get('/paleta', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'paleta.latte');
})->setName('paleta');

$app->get('/kontakt', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'kontakt.latte');
})->setName('kontakt');

$app->get('/piskvorky', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'piskvorky.latte');
})->setName('piskvorky');

$app->get('/hra2048', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'hra2048.latte');
})->setName('hra2048');

$app->get('/test', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'test.latte');
})->setName('test');

$app->post('/test-vyhodnoceni', function (Request $request, Response $response, $args){
    $data = $request->getParsedBody();
    $body = 0;

    for ($i = 1; $i<=10; $i++){
        if ($data[$i] === 'a') {
            $body = $body + 1;
        }
    }

    $tplVars['body'] = $body;
    return $this->view->render($response, 'test-vyhodnoceni.latte', $tplVars);
})->setName('test-vyhodnoceni');