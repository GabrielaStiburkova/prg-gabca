<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Kontaktní formulář</title>
    </head>
    <body>
        <p style="font-weight: bold">Můžete mě kontaktovat přes formulář níže </p>

        <form method="POST">
            <table>
                <tr>
                    <td style="font-weight: bolder"> Jak se jmenujete ?</td>
                    <td><input name="jmeno" type="text" /></td>
                </tr>
                <tr>
                    <td style="font-weight: bolder">Jaký je Váš email ?</td>
                    <td><input name="email" type="email" /></td>
                </tr>
        <tr>
                    <td style="font-weight: bolder">Jaký je aktuální rok ?</td>
                    <td><input name="rok" type="number" /></td>
                </tr>
            </table>
            <textarea name="zprava"></textarea><br>

            <input type="submit" value="Odeslat" />
        </form>

        <?php
        mb_internal_encoding("UTF-8");

        // VALIDACE

        $hlaska = '';

        if ($_POST)    // V poli _POST něco je = odeslal se formulář
        {
            if (isset($_POST['jmeno']) && $_POST['jmeno'] &&
                isset($_POST['email']) && $_POST['email'] &&
                isset($_POST['zprava']) && $_POST['zprava'] &&
                isset($_POST['rok']) && $_POST['rok'] == date('Y'))
            {
        }
        else
            $hlaska = 'Váš formulář není správně vyplněný! Zkuste to znovu.';
        }
        // funkce na odesílání

        $hlavicka = 'From:' . $_POST['email'];
        $hlavicka = "\nMIME-Version: 1.0\n";
        $hlavicka = "Content-Type: text/html; charset=\"utf-8\"\n";
        $adresa = 'gabriela.stiburkova@educanet.cz';
        $predmet = 'Nová zpráva z mailformu- web o gabi';
        $uspech = mb_send_mail($adresa, $predmet, $_POST['zprava'], $hlavicka);

        if ($uspech)
        {
        $hlaska = 'Email byl úspěšně odeslán, co nejdříve vám odpovím.';
    }
    else
    {
        $hlaska = 'Email se nepodařilo odeslat. Zkontrolujte prosím svoji adresu.';
	    }
	?>

    </body>
</html>