import java.util.Random;
import java.util.List;
import java.util.ArrayList;

public class bubblesort{

    public static void main(String[] args) {
        Random randomCislo = new Random();
        int[] listCisel = new int [20];
        {
            listCisel[0] = randomCislo.nextInt(50);
            listCisel[1] = randomCislo.nextInt(50);
            listCisel[2] = randomCislo.nextInt(50);
            listCisel[3] = randomCislo.nextInt(50);
            listCisel[4] = randomCislo.nextInt(50);
            listCisel[5] = randomCislo.nextInt(50);
            listCisel[6]= randomCislo.nextInt(50);
            listCisel[7] = randomCislo.nextInt(50);
            listCisel[8] = randomCislo.nextInt(50);
            listCisel[9] = randomCislo.nextInt(50);
            listCisel[10] = randomCislo.nextInt(50);
            listCisel[11] = randomCislo.nextInt(50);
            listCisel[12] = randomCislo.nextInt(50);
            listCisel[13] = randomCislo.nextInt(50);
            listCisel[14] = randomCislo.nextInt(50);
            listCisel[15] = randomCislo.nextInt(50);
            listCisel[16] = randomCislo.nextInt(50);
            listCisel[17] = randomCislo.nextInt(50);
            listCisel[18] = randomCislo.nextInt(50);
            listCisel[19] = randomCislo.nextInt(50);

            System.out.print("Vygenerovaná čísla jsou: ");
            bubblesort(listCisel) ;
            System.out.print("Čísla po setřídění: ");
        }
    }

    public static void bubblesort (int[] list) {
        int j = list.length -2, temp;
        boolean swapped = true;
        while (swapped) {
            swapped = false;
            for (int i=0 ; i <= j; i++) {
                if (list[i] > list[i+1]) {
                    temp = list[i];
                    list[i] = list[i +1];
                    list[i +1] = temp;
                    swapped=true;
                }
            }
        }   j++;

    }

}

