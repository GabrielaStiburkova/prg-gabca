package cz.educanet.farma;

import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;

import static java.lang.Integer.valueOf;

public class HlavniOkno extends JFrame {

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    private JLabel labKralici = new JLabel();
    private JLabel labHusy = new JLabel();
    private JLabel labEvidence = new JLabel();
    private JLabel labKrmivo = new JLabel();
    private JLabel labKraliciSamci = new JLabel();
    private JLabel labKraliciSamice = new JLabel();
    private JLabel labHusySamci = new JLabel();
    private JLabel labHusySamice = new JLabel();
    private JLabel labChov = new JLabel();
    private JLabel labKraliciChov = new JLabel();
    private JLabel labHusyChov = new JLabel();
    private JLabel labMrkev = new JLabel();
    private JLabel labPsenice = new JLabel();
    private JButton btnVypocti = new JButton();
    private JTextField txtKraliciSamci = new JTextField();
    private JTextField txtKraliciSamice = new JTextField();
    private JTextField txtHusySamci = new JTextField();
    private JTextField txtHusySamice = new JTextField();
    private JLabel labMusiteVypestovat = new JLabel();

    public void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown

        //======== this ========
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Farm\u00e1\u0159ka 2.0");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        setSize(605, 450);
        setLocationRelativeTo(null);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents

        //---- labKralici ----
        labKralici.setText("Kr\u00e1l\u00edci");
        labKralici.setFont(new Font("Arial", Font.PLAIN, 20));
        contentPane.add(labKralici);
        labKralici.setBounds(25, 55, 100, 20);

        //---- labHusy ----
        labHusy.setText("Husy");
        labHusy.setFont(new Font("Arial", Font.PLAIN, 20));
        contentPane.add(labHusy);
        labHusy.setBounds(new Rectangle(new Point(300, 60), labHusy.getPreferredSize()));

        //---- labEvidence ----
        labEvidence.setText("Evidence kr\u00e1l\u00edk\u016f a hus");
        labEvidence.setFont(new Font("Times New Roman", Font.BOLD, 24));
        labEvidence.setEnabled(false);
        contentPane.add(labEvidence);
        labEvidence.setBounds(25, 25, 380, 25);

        //---- labKrmivo ----
        labKrmivo.setText("Pot\u0159eba krmiva ");
        labKrmivo.setFont(new Font("Times New Roman", Font.ITALIC, 24));
        contentPane.add(labKrmivo);
        labKrmivo.setBounds(25, 290, labKrmivo.getPreferredSize().width, 25);

        //---- labKraliciSamci ----
        labKraliciSamci.setText("Po\u010det samc\u016f");
        contentPane.add(labKraliciSamci);
        labKraliciSamci.setBounds(new Rectangle(new Point(25, 95), labKraliciSamci.getPreferredSize()));

        //---- labKraliciSamice ----
        labKraliciSamice.setText("Po\u010det samic");
        contentPane.add(labKraliciSamice);
        labKraliciSamice.setBounds(new Rectangle(new Point(25, 125), labKraliciSamice.getPreferredSize()));

        //---- labHusySamci ----
        labHusySamci.setText("Po\u010det samc\u016f");
        contentPane.add(labHusySamci);
        labHusySamci.setBounds(250, 80, 70, 25);

        //---- labHusySamice ----
        labHusySamice.setText("Po\u010det samic");
        contentPane.add(labHusySamice);
        labHusySamice.setBounds(250, 80, 70, 25);

        //---- labChov ----
        labChov.setText("Velikost chovu p\u0159ed zimou:");
        labChov.setFont(new Font("Arial", Font.PLAIN, 20));
        contentPane.add(labChov);
        labChov.setBounds(25, 205, labChov.getPreferredSize().width, 24);
        contentPane.add(labKraliciChov);
        labKraliciChov.setBounds(25, 205, 180, 24);
        contentPane.add(labHusyChov);
        labHusyChov.setBounds(25, 205, 180, 24);

        //---- labMrkev ----
        labMrkev.setForeground(Color.red);
        contentPane.add(labMrkev);
        labMrkev.setBounds(25, 350, 540, 24);

        //---- labPsenice ----
        labPsenice.setForeground(Color.blue);
        contentPane.add(labPsenice);
        labPsenice.setBounds(25, 350, 540, 24);

        //---- btnVypocti ----
        btnVypocti.setText("Vypo\u010dti");
        btnVypocti.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnVypocti.addActionListener(e -> poKliknutiVypocti(e));
        contentPane.add(btnVypocti);
        btnVypocti.setBounds(30, 200, 550, btnVypocti.getPreferredSize().height);
        contentPane.add(txtKraliciSamci);
        txtKraliciSamci.setBounds(130, 100, 110, 25);
        contentPane.add(txtKraliciSamice);
        txtKraliciSamice.setBounds(130, 100, 110, 25);
        contentPane.add(txtHusySamci);
        txtHusySamci.setBounds(410, 100, 100, 25);
        contentPane.add(txtHusySamice);
        txtHusySamice.setBounds(410, 100, 100, 25);

        //---- labMusiteVypestovat ----
        labMusiteVypestovat.setText("P\u0159ed zimou mus\u00edte vyp\u011bstovat:");
        contentPane.add(labMusiteVypestovat);
        labMusiteVypestovat.setBounds(new Rectangle(new Point(25, 320), labMusiteVypestovat.getPreferredSize()));

    }

    private void poKliknutiVypocti(ActionEvent e) {
        // Nejprve vytvoříme proměnné, do kterých se uloží hodnoty, které zadá uživatel.
        String sKraliciPocetSamic = txtKraliciSamice.getText();
        String sKraliciPocetSamcu = txtKraliciSamci.getText();
        String sHusyPocetSamic = txtHusySamice.getText();
        String sHusyPocetSamcu = txtHusySamci.getText();

        // Poté je převedeme na čísla, abychom s nimi mohli počítat.
        Integer iKraliciPocetSamic = valueOf(sKraliciPocetSamcu);
        Integer iKraliciPocetSamcu = valueOf(sKraliciPocetSamcu);
        Integer iHusyPocetSamic = valueOf(sHusyPocetSamic);
        Integer iHusyPocetSamcu = valueOf(sHusyPocetSamcu);

        // A provedeme výpočty, ke kterým si vytvoříme proměnné. Nesmíme ale zapomenout ohlídat
        // pokud v chovu nemáme zastoupeny obě pohlaví. Výpočty zobrazíme v příslušných labelech,
        // po převodu zpět na Stringy.
        Integer iKraliciPocetMladat = iKraliciPocetSamic * 4 * 10;
        Integer iHusyPocetMladat = iHusyPocetSamic * 15;
        Integer iKraliciVelikostChovu;
        Integer iHusyVelikostChovu;

        if (iKraliciPocetSamic > 0 && iKraliciPocetSamcu > 0) {
            iKraliciVelikostChovu = iKraliciPocetMladat + iKraliciPocetSamcu + iKraliciPocetSamic;
        } else {
            iKraliciVelikostChovu = iKraliciPocetSamcu + iKraliciPocetSamic;
        }

        if (iHusyPocetSamic > 0 && iHusyPocetSamcu > 0) {
            iHusyVelikostChovu = iHusyPocetMladat + iHusyPocetSamcu + iHusyPocetSamic;
        } else {
            iHusyVelikostChovu = iHusyPocetSamcu + iHusyPocetSamic;
        }

        Double dKgMrkve = iKraliciVelikostChovu * 183 * 0.5D;
        Double dRadkyMrkve = dKgMrkve / 5;
        Double dKgPsenice = iHusyVelikostChovu * 183 *0.25D;
        Double dRadkyPsenice = dKgPsenice / 2;

        String sKraliciChov = iKraliciVelikostChovu.toString();
        String sHusyChov = iHusyVelikostChovu.toString();
        String sKgMrkve = dKgMrkve.toString();
        String sKgPsenice = dKgPsenice.toString();
        String sRadkyMrkve = dRadkyMrkve.toString();
        String sRadkyPsenice = dRadkyPsenice.toString();

        labKraliciChov.setText(sKraliciChov + " ks králíků");
        labHusyChov.setText(sHusyChov + " ks hus");
        labMrkev.setText(sKgMrkve + " kg mrkve, tedy " + sRadkyMrkve + " řádků.");
        labPsenice.setText(sKgPsenice + " kg pšenice, tedy " + sRadkyPsenice + " řádků.");
    }


}