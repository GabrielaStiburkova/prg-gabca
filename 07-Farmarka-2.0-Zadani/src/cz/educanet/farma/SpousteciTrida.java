package cz.educanet.farma;

import javax.swing.*;
import net.sevecek.util.swing.*;

public class SpousteciTrida {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(SpousteciTrida::otevriOkno);
    }

    private static void otevriOkno() {
        SwingExceptionHandler.install();
        HlavniOkno okno = new HlavniOkno();
        okno.initComponents();
        okno.setVisible(true);
    }

}
