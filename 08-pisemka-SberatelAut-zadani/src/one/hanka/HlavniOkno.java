package one.hanka;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

import static java.lang.Integer.valueOf;

public class HlavniOkno  extends JFrame {
    JLabel labEvidence = new JLabel();
    JLabel labAuta = new JLabel();
    JLabel labMotorky = new JLabel();
    JLabel labPalivo = new JLabel();
    JLabel labspotrebaaut = new JLabel();
    JLabel labspotrebamotorek = new JLabel();
    JLabel labcelkovaspotreba = new JLabel();
    JLabel labpneumatiky = new JLabel();
    JLabel labvozidla = new JLabel();
    JLabel labstaj = new JLabel();
    JLabel labpocetaut = new JLabel();
    JLabel labpocetmotorek = new JLabel();
    JTextField txtpocetaut = new JTextField();
    JTextField txtpocetmotorek = new JTextField();
    JTextField txtprumernaspotrebamotorek = new JTextField();
    JTextField txtprumernaspotrebaaut = new JTextField();
    JButton btnVypocti = new JButton();
//TODO: Dopiš globální proměnné. POZOR! Jen ty co opnravdu potřebuješ globálně

    public void initComponents() {

     /* začátek přednastavených komponent */



        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Sberatel aut");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        setSize(605, 450);
        setLocationRelativeTo(null);
        // nastavení komponent
        {
            contentPane.add(labEvidence);
            labEvidence.setBounds(20, 20, 370, 20);
            labEvidence.setText("Evidence aut a motorek");
            labEvidence.setFont(new Font("Monotype Corsiva", Font.BOLD, 22));

            contentPane.add(labAuta);
            labAuta.setBounds(20, 50, 100, 20);
            labAuta.setText("Auta");

            contentPane.add(labMotorky);
            labMotorky.setText("Motorky");
            labMotorky.setFont(new Font("Segoe UI", Font.PLAIN, 18));
            labMotorky.setBounds(300, 50, 100, 20);

            contentPane.add(labPalivo);
            labPalivo.setBounds(20, 285, 150, 20);
            labPalivo.setText("Celková spotřeba");

            JLabel labPocetAut = new JLabel();
            labPocetAut.setText("Počet aut");

            contentPane.add(labspotrebaaut);
            labspotrebaaut.setText("Průmerná spotřeba");
            labspotrebaaut.setBounds(30, 120, 150, 20);

            contentPane.add(labspotrebamotorek);
            labspotrebamotorek.setText("Průměrná spotřeba");
            labspotrebamotorek.setBounds(420, 120, 150, 20);

            contentPane.add(labcelkovaspotreba);
            labcelkovaspotreba.setText("Celková spotřeba aut");
            labcelkovaspotreba.setBounds(420, 120, 150, 20);

            contentPane.add(labpneumatiky);
            labpneumatiky.setText("Počet pneumatik");
            labpneumatiky.setBounds(20, 258, 150, 20);
            labpneumatiky.setForeground(Color.blue);


            contentPane.add(labvozidla);
            labvozidla.setText("Počet vozidel");
            labvozidla.setBounds(20, 120, 150, 20);
            labvozidla.setForeground(Color.red);

            contentPane.add(labstaj);
            labvozidla.setText("Počet strojů ve stáji");
            labvozidla.setBounds(20, 258, 150, 20);

            contentPane.add(btnVypocti);
            btnVypocti.setBounds(20, 150, 545, btnVypocti.getPreferredSize().height);
            btnVypocti.setText("Vypočítej");
            btnVypocti.addActionListener(e -> poKliknutiVypocti(e));
            btnVypocti.setFont(new Font("Times New Roman", Font.PLAIN, 25));

            contentPane.add(txtpocetaut);
            txtpocetaut.setBounds(130, 100, 110, 20);

            contentPane.add(txtpocetmotorek);
            txtpocetmotorek.setBounds(130,100, 110, 20);

            contentPane.add(txtprumernaspotrebaaut);
            txtprumernaspotrebaaut.setBounds(130,100,110,20);

            contentPane.add(txtprumernaspotrebamotorek);
            txtprumernaspotrebamotorek.setBounds(130,100,110,20);


        }
        // vypočet preferované velikosti
        {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
     /* konec přednastavených komponent */

        //TODO: Dopiš nastavení komponent dle sceenshotu aplikace


        //TODO: Dopiš metodu po kliknutí dle sceenshotu aplikace. Celá stáj najede za den průměrně 98 km denně (každé vozidlo). Pneu měníme každý rok.






    }

    private void poKliknutiVypocti(ActionEvent e) {
        String spocetaut = txtpocetaut.getText();
        String spocetmotorek = txtpocetmotorek.getText();
        String sprumumernaspotrebamotorek = txtprumernaspotrebamotorek.getText();
        String sprumernaspotrebaaut = txtprumernaspotrebaaut.getText();

        Integer ipocetaut = valueOf(spocetaut);
        Integer ipocetmotorek = valueOf(spocetmotorek);
        Integer iprumernaspotrebamotorek = valueOf(sprumumernaspotrebamotorek);
        Integer iprumernaspotrebaaut = valueOf(sprumernaspotrebaaut);
        Double palivoAuta = iprumernaspotrebaaut * 95 * 365.0 /100;
        Double palivoMotorky = iprumernaspotrebamotorek * 95 * 365.0 / 100;
        Double PalivoCelkem = palivoAuta + palivoMotorky;



    }
}
