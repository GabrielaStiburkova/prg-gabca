package cz.Gabca.Test;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class HlavniOkno extends JFrame {

    JLabel labdolary = new JLabel();
    JLabel labkoruny = new JLabel();
    JTextField txtpocetUSD = new JTextField();
    JTextField txtCZKnaUSD = new JTextField();
    JButton btnVypocti = new JButton();

    public void nastavOkno() {

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Převod USD na CZK");
        setSize(800,500);
        setLocationRelativeTo(null);
        Container plocha = getContentPane();
        plocha.setLayout(null);

        JLabel labdolary = new JLabel();
        labdolary.setText("Kolik máš dolarů ?");
        labdolary.setBounds(180,50,500,50);
        labdolary.setFont(new Font("Times New Roman",Font.BOLD ,20));
        labdolary.setForeground(Color.green);
        plocha.add(labdolary);

        JTextField txtpocetUSD = new JTextField();
        txtpocetUSD.setBounds(280,110,100,30);
        plocha.add(txtpocetUSD);

        JTextField txtCZKzaUSD = new JTextField();
        txtCZKzaUSD.setBounds(280,180,100,30);
        plocha.add(txtCZKzaUSD);

        JLabel labkoruny = new JLabel();
        labkoruny.setText("Kolik dostaneš CZK za 1 USD ?");
        labkoruny.setBounds(100,100,500,50);
        labkoruny.setFont(new Font("Times New Roman",Font.BOLD, 20));
        labkoruny.setForeground(Color.blue);
        plocha.add(labkoruny);

        JButton btnVypocti = new JButton();
        plocha.add(btnVypocti);
        btnVypocti.setBounds(150, 100,150,25);
        btnVypocti.setText("Vypočti");
        btnVypocti.setFont(new Font("Arial", Font.BOLD, 32));
        btnVypocti.addActionListener(e -> poKliknutiVypocti(e));

    }

    private void poKliknutiVypocti(ActionEvent e) {

        int txtUSDnaCZK = Integer.parseInt(labdolary.getText()) * Integer.parseInt(labkoruny.getText()));
        String println;("Celkem dostaneš"txtUSDnaCZK"CZK");

        double txtpocetUSD = Integer.parseInt(labdolary.getText()) * 23.30;
        String print("Při stálém kurzu 1:23,30 by to bylo "txtpocetUSD"CZK");


    }

}

