<?php
mb_internal_encoding("UTF-8");

$hlaska = '';
if ($_POST) // V poli _POST něco je, odeslal se formulář
{
    if (isset($_POST['jmeno']) && $_POST['jmeno'] &&
        isset($_POST['email']) && $_POST['email'] &&
        isset($_POST['zprava']) && $_POST['zprava'] &&
        isset($_POST['rok']) && $_POST['rok'] == date('Y'))
    {
        $hlavicka = 'From:' . $_POST['email'];
        $hlavicka .= "\nMIME-Version: 1.0\n";
        $hlavicka .= "Content-Type: text/html; charset=\"utf-8\"\n";
        $adresa = 'gabriela.stiburkova@educanet.cz';
        $predmet = 'Nová zpráva z mailformu(Gabi)';
        $uspech = mb_send_mail($adresa, $predmet, $_POST['zprava'], $hlavicka);
        if ($uspech)
        {
            $hlaska = 'Tvůj e-mail se podařilo úspěšně odeslat.';
        }
        else
            $hlaska = 'Tvůj e-mail se bohužel nepodařilo odeslat. Zkontroluj si zadané údaje a zkus to znovu.';
    }
    else
        $hlaska = 'Tvůj formulář nebyl správně vyplněný. Oprav si své chyby !';
}

?>

<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="public/css/styly.css">

    <link rel="shortcut icon" href="public/favicon.ico" type="image/x-icon">
    <link rel="icon" href="public/favicon.ico" type="image/x-icon">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Web k výuce - kontakt</title>
</head>
<body>
<div class="page">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="{link index}">Web k výuce</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link" href="{link index}">Hlavní stránka</a>
                    <a class="nav-item nav-link" href="{link kontakt}">Formulář</a>
                </div>
            </div>
        </nav>

        <div class="container">

            <h1 style="font-weight: bold">Kontakt</h1>
            <p style="font-family: 'Times New Roman'">Gabriela Stibůrková<br>
                E-mail: gabriela.stiburkova@educanet.cz<br>
            </p>

            <h2 style="font-weight: bold">Kontaktní formulář</h2>
            <p>Pokud mě chceš kontaktovat přímo tady z webu, využij následující formulář níže.</p>

            <?php
            if ($hlaska)
                echo('<p>' . $hlaska . '</p>');
            ?>

            <form method="POST">
                <table>
                    <tr>
                        <td style="font-weight: bolder">Zde zadej své jméno</td>
                        <td><input name="jmeno" type="text" /></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder">Zde zadej svůj e-mail</td>
                        <td><input name="email" type="email" /></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder">Zde napiš jaký je nyní rok?</td>
                        <td><input name="rok" type="number" /></td>
                    </tr>
                </table>
                <textarea name="zprava"></textarea><br />

                <input type="submit" value="Odeslat" />
            </form>

        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>