<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/index', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'index.latte');
})->setName('Web o Gabi');

$app->get('/kontakt', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'kontakt.latte');
})->setName('kontakt');

$app->get('/Paleta', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'Paleta.latte');
})->setName('Web o výběru palety');

$app->get('/Letadla', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'Letadla.latte');
})->setName('Web o Letadlech');

$app->get('/Cestování', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'Cestování.latte');
})->setName('Web o cestování');

$app->get('/Anime', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'Anime.latte');
})->setName('Web o anime');