package cz.educanet.recept;

import cz.educanet.recept.naradi.*;
import cz.educanet.recept.suroviny.*;

public class SpousteciTrida {

    public static void main(String[] args) {
        Miska cervenaMiska = new Miska();
        Miska zlutaMiska = new Miska();
        Mixer mixer = new Mixer();
        KuchynskaVaha vaha = new KuchynskaVaha();
        PlechNaPeceni plech = new PlechNaPeceni();
        ElektrickaTrouba trouba = new ElektrickaTrouba();

        Vajicka platoVajec = new Vajicka();
        Cukr pytlikCukru = new Cukr();
        Mouka pytlikMouky = new Mouka();
        Maslo maslo125g = new Maslo();
        PrasekDoPeciva prasekDoPeciva = new PrasekDoPeciva();
        Ovoce kosikOvoce = new Ovoce();

        //---------------------------------------------------------------------

        for(int i =0; i<4; i=i+1)
        {
            cervenaMiska.nalozJedenKus(platoVajec);
        }
        cervenaMiska.nalozTrochu(pytlikCukru);
        mixer.zamichej(cervenaMiska);
        zlutaMiska.nalozTrochu(pytlikMouky);
        while(vaha.zjistiHmotnost(zlutaMiska) != 250)
        {
            if(vaha.zjistiHmotnost(zlutaMiska)>250)
            {
                zlutaMiska.vylozTrochu();
            }
            if(vaha.zjistiHmotnost(zlutaMiska)<250)
            {
                zlutaMiska.nalozTrochu(pytlikMouky);
            }
        }
        cervenaMiska.nalozCelyObsah(zlutaMiska);
        cervenaMiska.nalozJedenKus(maslo125g);
        cervenaMiska.nalozCelyObsah(prasekDoPeciva);
        mixer.zamichej(cervenaMiska);
        plech.nalozCelyObsah(cervenaMiska);
        for(int i=0; i<50; i=i+1)
        {
            plech.nalozJedenKus(kosikOvoce);
        }
        trouba.zapniSe(180);
        trouba.pec(10);
        trouba.vlozSiDovnitr(plech);
        trouba.pec(25);
        trouba.vypniSe();
        trouba.vyndejObsahVen();
    }

}
